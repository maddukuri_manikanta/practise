package com.geektrust;

public class DataUtils {

    public static Person loadDefaultFamilyDataForKing(Person king){

        Person queen = new Person("Queen Anga",Gender.FEMALE);
        king.setPartner(queen);
//        queen.setPartner(king);

        Person chit = new Person("Chit",Gender.MALE);
        king.addChildren(chit);

        Person amba = new Person("Amba",Gender.FEMALE);
        chit.setPartner(amba);

        Person dritha = new Person("Dritha",Gender.FEMALE);
        amba.addChildren(dritha);

        Person jaya = new Person("Jaya",Gender.FEMALE);
        dritha.setPartner(jaya);

        Person tritha = new Person("Tritha",Gender.FEMALE);
        amba.addChildren(tritha);
        Person vritha = new Person("Vritha",Gender.MALE);
        amba.addChildren(vritha);
        Person yodhan = new Person("Yodhan",Gender.MALE);
        jaya.addChildren(yodhan);

        Person ish = createPerson("Ish",Gender.MALE);
        king.addChildren(ish);

        Person vich = createPerson("Vich",Gender.MALE);
        king.addChildren(vich);

        Person lika = createPerson("lika",Gender.FEMALE);
        lika.setPartner(vich);

        Person vila = createPerson("Vila",Gender.FEMALE);
        lika.addChildren(vila);
        Person chika = createPerson("Chika",Gender.FEMALE);
        lika.addChildren(chika);


        Person aras = createPerson("Aras",Gender.MALE);
        king.addChildren(aras);

        Person chitra = createPerson("Chitra",Gender.FEMALE);
        chitra.setPartner(aras);

        Person jnki = createPerson("Jnki",Gender.FEMALE);
        chitra.addChildren(jnki);

        Person arit = createPerson("Arit",Gender.MALE);
        arit.setPartner(jnki);

        Person ahit = createPerson("Ahit",Gender.MALE);
        chitra.addChildren(ahit);

        Person laki = createPerson("Laki",Gender.MALE);
        jnki.addChildren(laki);

        Person lavnya = createPerson("Lavnya",Gender.FEMALE);
        jnki.addChildren(lavnya);

        Person satya = createPerson("Satya",Gender.FEMALE);
        king.addChildren(satya);

        Person vyan = createPerson("Vyan",Gender.MALE);
        vyan.setPartner(satya);

        Person asva = createPerson("Asva",Gender.MALE);
        satya.addChildren(asva);

        Person satvy = createPerson("Satvy",Gender.FEMALE);
        satvy.setPartner(asva);

        Person krpi = createPerson("Krpi",Gender.FEMALE);
        satya.addChildren(krpi);

        Person vyas = createPerson("Vyas",Gender.FEMALE);
        satya.addChildren(vyas);

        Person atya = createPerson("Atya",Gender.FEMALE);
        satya.addChildren(atya);

        Person vasa = createPerson("Vasa",Gender.MALE);
        satvy.addChildren(vasa);

        Person kriya = createPerson("Kriya",Gender.MALE);
        krpi.addChildren(kriya);

        Person krithi = createPerson("Krithi",Gender.FEMALE);
        krpi.addChildren(krithi);

        return king;
    }

    private static Person createPerson(String name,Gender gender){
        return new Person(name,gender);
    }
}
