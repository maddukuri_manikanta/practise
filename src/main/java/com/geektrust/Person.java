package com.geektrust;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Person {
    private String name;

    // has to be modified to enum
    private Gender gender;

    private Person parent;

    private Person partner;

    private List<Person> children = new ArrayList<>();

    public Person() {
    }

    public Person(String name, Gender gender) {
        this.name = name;
        this.gender = gender;
    }

    public Person(String name, Gender gender, Person partner, List<Person> children) {
        this.name = name;
        this.gender = gender;
        this.partner = partner;
        this.children = children;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Person getParent() {
        return parent;
    }

    public void setParent(Person parent) {
        this.parent = parent;
    }

    public Person getPartner() {
        return partner;
    }

    public void setPartner(Person partner) {
        this.partner = partner;
        partner.partner = this;


    }


    public List<Person> getChildren() {

        if((this.children !=null) && (!this.children.isEmpty())){
            return this.children;
        }else if( (this.partner!=null) && ((this.partner.children != null)) && (!this.partner.children.isEmpty()) ){
            return this.partner.children;
        }
        return children;
    }

    public void setChildren(List<Person> children) {
        this.children = children;
    }

    public void addChildren(Person person){
        person.setParent(this);
        if(this.children == null){
            this.children = new ArrayList<Person>(Arrays.asList(person));
        }else{
            this.children.add(person);
        }
    }

//    @Override
//    public String toString() {
//        return "Person{" +
//                "name='" + name + '\'' +
//                ", gender='" + gender + '\'' +
//                ", partner=" + partner.getName() +
//                '}';
//    }

//    @Override
//    public String toString() {
//        return "Person{" +
//                "name='" + name + '\'' +
//                ", gender='" + gender + '\'' +
//                ", children=" + children +
//                '}';
//    }


    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", partner=" + (( partner ==null) ? (" ") : (partner.getName())) +
                ", children=" + children +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;
        Person person = (Person) o;
        return Objects.equals(name, person.name) &&
                Objects.equals(gender, person.gender) &&
                Objects.equals(parent, person.parent) &&
                Objects.equals(partner, person.partner) &&
                Objects.equals(children, person.children);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, gender, parent, partner, children);
    }
}
