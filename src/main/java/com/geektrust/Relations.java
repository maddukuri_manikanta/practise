package com.geektrust;

import com.problemsolving.practise.trees.Node;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Relations {

    public static  void addChild(Person child,Person parent){

    }

    public void getRelatives(Relationships relationship,Person person){

        switch (relationship){
            case SON:
                getSons(person);
                break;
            case DAUGHTER:
                break;
            case SIBLINGS:
                break;
            case PATERNAL_UNCLE:
                break;
            case MATERNAL_UNCLE:
                break;
            case PATERNAL_AUNT:
                break;
            case MATERNAL_AUNT:
                break;
            case BROTHRE_IN_LAW:
                break;
            case SISTER_IN_LAW:
                break;
            default:
                System.out.println("Relationship not found");
                break;
        }

    }

    public Person searchPerson(String name,Person root){

        System.out.println(" looking for node : "+ root.getName());
        try {
            if (root.getName().equalsIgnoreCase(name)) {
                return root;
            }

            if ((root.getPartner() != null)) {
                if (root.getPartner().getName().equalsIgnoreCase(name)) {
                    return root.getPartner();
                }
            }

            if (root.getChildren() != null) {
                Person result = null;
                for (Person child :
                        root.getChildren()) {
                    result = searchPerson(name, child);

                    if(result!=null){
                        break;
                    }
                }
                return result;
            }

        }catch (Exception e){
            System.err.println(e.getMessage() + " for "+root.getName());
        }
        return  null;
    }

    private List<Person> getSons(Person person){
        List<Person> sons = new ArrayList<>();

        for (Person child:
             person.getChildren()) {
            if(child.getGender() == Gender.MALE){
                sons.add(child);
            }
        }
        return sons;
    }

    private List<Person> getDaughters(Person person){
        List<Person> daughters = new ArrayList<>();

        for (Person child:
             person.getChildren()) {
            if(child.getGender() == Gender.FEMALE){
                daughters.add(child);
            }
        }
        return daughters;
    }

    private List<Person> getPaternalUncle(Person person){
        List<Person> uncles = new ArrayList<>();

        // dad = person.getParent
        Person father = person.getParent();

        // dad.getParent.getSons except dad
        for (Person person1:
            father.getParent().getChildren()) {
            if (person1.getGender() == Gender.MALE){
                uncles.add(person1);
            }
        }
        return  uncles;
    }

    private List<Person> getSisterInLaw(Person person){
        List<Person> inLaws = new ArrayList<>();

        // person has to be Male
        // get partner then -> use java 8 optionals
        Person partner = person.getPartner();

        // find Partners sister -> use common siblings method and then filter using java 8 streams
        for (Person person1:
        partner.getParent().getChildren()) {
            if(!person1.equals(person) && (person.getGender() == Gender.FEMALE)){
                inLaws.add(person1);
            }
        }
        return inLaws;
    }

    private List<Person> getBrotherInLaws(Person person){
        List<Person> inLaws = new ArrayList<>();

        // 1. spouse brother
        Person partner = person.getPartner();
        inLaws.addAll(getSiblings(partner).stream().filter( child -> child.getGender() == Gender.MALE).collect(Collectors.toList()));

        // 2. husbands of siblings
        inLaws.addAll(person.getParent().getChildren().stream().filter( child -> {
            if(child.getGender() == Gender.FEMALE){
                if(child.getPartner() != null){
                   return true;
                }
            }
            return  false;
        } ).map(Person::getPartner).collect(Collectors.toList()));
//        inLaws.addAll(person.getParent().getChildren().stream().filter( child1 -> !child1.equals(person)).collect(Collectors.toCollection(Collectors.toList())));

        return  inLaws;
    }

    // common generic method for getting siblings
    private List<Person> getSiblings(Person person){
        List<Person> siblings = new ArrayList<>();

        person.getParent().getChildren();

        for (Person child:
             person.getParent().getChildren()) {
            if (!child.equals(person)){
                siblings.add(child);
            }
        }
        return siblings;
    }
}
