package com.geektrust;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
//        Person king = new Person("king shan",Gender.FEMALE);

        DataUtils dataUtils = new DataUtils();
        Person king = dataUtils.loadDefaultFamilyDataForKing(new Person("king shan",Gender.FEMALE));

//        System.out.println(king.toString());

        // test some methods
        Relations relations = new Relations();

        Person person = relations.searchPerson("krithi",king);

        System.out.println(person);

    }
}