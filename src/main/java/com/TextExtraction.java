package com;
import net.sourceforge.tess4j.*;
import java.io.*;
public class TextExtraction {
    public String getImgText(String imageLocation) {
      ITesseract instance = new Tesseract();
      try 
      {
    	 instance.setDatapath("G:\\manikanta");
    	 instance.setLanguage("eng");
    	 File file = new File(imageLocation);
         String imgText = instance.doOCR(file);
         return imgText;
      } 
      catch (TesseractException e) 
      {
         e.getMessage();
         return "Error while reading image";
      }
   }
   public static void main ( String[] args)
   {
      TextExtraction app = new TextExtraction();
      System.out.println(app.getImgText("C:\\Users\\ap_pa\\Desktop\\lake-anna-state-park.jpg"));
   }
}