package com.problemsolving.practise;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class TracxnProblem {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		int numberOfDays = in.nextInt();
		Map<String, List<String>> ingredients = new HashMap<String, List<String>>();

		for (int i = 0; i < numberOfDays; i++) {
			String ingredientId = in.next();
			String category = findCategory(ingredientId);

			// adding ingredients to hashmap
			if (ingredients.containsKey(category)) {
				List<String> ids = ingredients.get(category);
				ids.add(ingredientId);
				ingredients.put(category, ids);
			} else {
				List<String> ids = new ArrayList<String>();
				ids.add(ingredientId);
				ingredients.put(category, ids);
			}

			// evaluating whether chef can prepare dish for the day or not

			// 3 ingredients of same category are available in the hashmap
			if (ingredients.get(category).size() >= 3) {

				// chef will prepare the dish with 3 of same category
				for (int j = 0; j < 3; j++) {
					System.out.print(ingredients.get(category).get(j));
					if (j < 2) {
						System.out.print(":");
					}
				}

				// removing chef used ingredients from map

				for (int j = 0; j < 3; j++) {
					// remove chosen ingredient from category list
					ingredients.get(category).remove(0);
					// if there are no ingredients for the category pls remove
					// category from hashmap
					if (ingredients.get(category).size() <= 0) {
						ingredients.remove(category);
					}
				}
			}
			// 2 ingredients of same category and 1 ingredient of different
			// category ( size of ingredients must be > 3 )
			else if (ingredients.get(category).size() == 2 && ingredients.size() >= 2) {
				for (int j = 0; j < 2; j++) {
					System.out.print(ingredients.get(category).get(j));
					System.out.print(":");
				}

				// removing 2 used ingredients from map
				for (int j = 0; j < 2; j++) {

					// remove chosen ingredient from category list
					ingredients.get(category).remove(0);
					// if there are no ingredients for the category pls remove
					// category from hashmap
					if (ingredients.get(category).size() <= 0) {
						ingredients.remove(category);
					}
				}

				// picking the 1 ingredient
				for (String othercategory : ingredients.keySet()) {
					if (!category.equals(othercategory)) {
						System.out.print(ingredients.get(othercategory).get(0));
						ingredients.get(othercategory).remove(0);
						break;
					}
				}

			} else {
				System.out.print("-");
			}

		}
		in.close();

	}

	static String findCategory(String ingredientId) {
		String category = "";
		if (ingredientId.startsWith("FIBER")) {
			category = "FIBER";
		} else if (ingredientId.startsWith("FAT")) {
			category = "FAT";
		} else if (ingredientId.startsWith("CARB")) {
			category = "CARB";
		}
		return category;
	}
}
