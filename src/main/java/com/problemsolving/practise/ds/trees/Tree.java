package com.problemsolving.practise.ds.trees;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@SuppressWarnings("unused")
class Node<T> {

	private T data;
	private List<Node<T>> children = new ArrayList<>();
	private Node<T> parent;

	Node(T data) {
		this.data = data;
	}

	Node<T> addChild(Node<T> child) {
		child.parent = this;
		this.children.add(child);
		return child;
	}

	void addChildren(List<Node<T>> childrens) {
		childrens.forEach(node -> {
			node.setParent(this);
		});
		this.children.addAll(childrens);
	}

	public List<Node<T>> getChildren() {
		return children;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public Node<T> getParent() {
		return parent;
	}

	void setParent(Node<T> parent) {
		this.parent = parent;
	}

}

@SuppressWarnings("unused")
public class Tree<T> {

	// for creating a sample tree
	public Node<String> createTree() {

		Node<String> node = new Node<String>("root");

		Node<String> node1 = node.addChild(new Node<String>("node1"));

		Node<String> node2 = node.addChild(new Node<String>("node2"));

		Node<String> node11 = node1.addChild(new Node<String>("node11"));
		Node<String> node12 = node1.addChild(new Node<String>("node12"));
		Node<String> node13 = node1.addChild(new Node<String>("node13"));
		Node<String> node14 = node1.addChild(new Node<String>("node14"));

		Node<String> node131 = node13.addChild(new Node<String>("node13_1"));

		Node<String> node21 = node2.addChild(new Node<String>("node21"));

		Node<String> node22 = node2.addChild(new Node<String>("node22"));

		return node;
	}

	// for printing tree nodes
	public void printTree(Node<T> node, String appender) {
		// print data
		if (node.getData() != null) {
			System.out.println(appender + node.getData());
		}

		// print children using recursion
		if (node.getChildren() != null) {
			node.getChildren().forEach(child -> printTree(child, appender + appender));
		}
	}

	// finding root node of tree from any given node
	Node<T> findRoot(Node<T> node) {
		if (node.getParent() == null) {
			return node;
		} else {
			return findRoot(node.getParent());
		}
	}

	// delete given node
	void deleteNode(Node<T> node){
		
	}
	
	public static void main(String[] args) {

		Tree<String> tree = new Tree<String>();
		Node<String> root = tree.createTree();
		tree.printTree(root, "---");

	}

}
