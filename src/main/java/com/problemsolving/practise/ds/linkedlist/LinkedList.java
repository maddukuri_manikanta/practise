package com.problemsolving.practise.ds.linkedlist;

public class LinkedList {

	Node head;

	static class Node {
		int data;
		Node next;

		Node(int data) {
			this.data = data;
		}

		Node(int data, Node next) {
			this.data = data;
			this.next = next;
		}
	}

	// print linked list nodes
	public static void printLinkedList(LinkedList list) {

		Node temp = list.head;

		while (temp != null) {
			System.out.print(temp.data + " -> ");
			temp = temp.next;
		}

		System.out.println();
	}

	// inserting at front of linked list
	public void addNodeFront(int data) {
		Node node = new Node(data);
		node.next = head;
		head = node;
	}

	// inserting node after given node
	public void addNode(Node node, int data) {
		Node newNode = new Node(data);
		newNode.next = node.next;
		node.next = newNode;
	}

	// inserting node at end
	public void addNodeEnd(int data) {

		Node newNode = new Node(data);

		if (head == null) {
			head = newNode;
			return;
		}

		Node pivot = head;

		while (pivot.next != null) {
			pivot = pivot.next;
		}

		pivot.next = newNode;

	}

	// delete node
	public void deleteNode(int data) {

		// if node is head node
		if (head.data == data) {
			head = head.next;
			return;
		}

		// or traverse and find prev and next node
		Node temp = head, prev = null;

		while (temp.next != null && temp.data != data) {
			prev = temp;
			temp = temp.next;
		}

		if (temp.data == data) {
			if (temp.next != null) {
				prev.next = temp.next;
			} else {
				prev.next = null;
			}
		} else {
			// throw some exception saying node not found
		}

	}

	/**
	 * @param position
	 */
	public void deleteNodeAtGivenPosition(int position) {

		if (position == 0) {
			head = head.next;
			return;
		}

		int counter = 0;

		Node prev = null, temp = head;
		while (counter < position && temp.next != null) {
			prev = temp;
			temp = temp.next;
			counter++;
		}

		if (counter == position) {
			prev.next = temp.next;
		} else {
			// throw exception saying index not found
		}
	}

	// in java its easy because of auto garbage collection otherwise we have to traverse through the each node by making previous node free
	public void deleteLinkedList() {
		head = null;
	}

	public static void main(String[] args) {
		LinkedList list = new LinkedList();

		list.head = new Node(1);

		Node node2 = new Node(2);
		Node node3 = new Node(4);

		list.head.next = node2;

		node2.next = node3;

		printLinkedList(list);

		list.addNodeFront(0);
		printLinkedList(list);

		list.addNode(node2, 3);
		printLinkedList(list);

		list.addNodeEnd(5);
		printLinkedList(list);

		list.deleteNode(4);
		printLinkedList(list);

		list.deleteNodeAtGivenPosition(3);
		printLinkedList(list);
	}

}
