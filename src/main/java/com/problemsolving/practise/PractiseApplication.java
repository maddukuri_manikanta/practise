package com.problemsolving.practise;

import java.util.Vector;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PractiseApplication {

	public static void main(String[] args) {
		SpringApplication.run(PractiseApplication.class, args);

		Vector<Integer> integers = new Vector<Integer>();

		integers.add(1);
	}

}
