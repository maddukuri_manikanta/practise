package com.problemsolving.practise.arrays;

import java.util.Arrays;
import java.util.BitSet;
import java.util.Iterator;
import java.util.Scanner;

public class UncoupledInteger {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		
		int[] arr = new int[]{1,5,3,4};
		
		BitSet bitset = new BitSet(5);
		
/*		for (int i : arr) {
			bitset.set(i, -1);
		}
		
		int missingIndex = 0;
		for(int i=0;i<1;i++){
			missingIndex = bitset.nextClearBit(missingIndex);
			System.out.println(++missingIndex);
		}*/
		
		
		printMissingNumber(arr, 5);
		
		in.close();
	}
	
	
	static int getMissingNo (int a[], int n) 
    { 
        int x1 = a[0];  
        int x2 = 1;  
          
        /* For xor of all the elements  
           in array */
        for (int i = 1; i < n; i++) 
            x1 = x1 ^ a[i]; 
                  
        /* For xor of all the elements  
           from 1 to n+1 */         
        for (int i = 2; i <= n+1; i++) 
            x2 = x2 ^ i;          
          
        return (x1 ^ x2); 
    } 
	
	private static void printMissingNumber(int[] numbers, int count) { 

		int missingCount = count - numbers.length; 
		BitSet bitSet = new BitSet(count); 
		for (int number : numbers) { 
			bitSet.set(number - 1);
		} 
		
		System.out.println(bitSet.get(0));
		System.out.printf("Missing numbers in integer array %s, with total number %d is %n", Arrays.toString(numbers), count); 
		int lastMissingIndex = 0; 
		for (int i = 0; i < missingCount; i++) { 
			lastMissingIndex = bitSet.nextClearBit(lastMissingIndex); 
			System.out.println(++lastMissingIndex); 
		} 
	}
}
