package com.problemsolving.practise.java.comparableandcomparator;

import java.util.Collections;
import java.util.HashMap;

public class Test {
    public static void main(String[] args) {
        HashMap<Employee,Integer> map = new HashMap<>();

        Employee emp1 = new Employee();
        emp1.setId(1);
        emp1.setName("test");

        map.put(emp1,1);


        Employee emp2 = new Employee(2,"emp2",new Address(1,"bangalore"));
        map.put(emp2,2);


        Employee emp3 = new Employee(3,"emp3",new Address(1,"bangalore"));
        map.put(emp3,2);
        map.put(emp3,2);

        for (Employee emp: map.keySet()) {
            System.out.println(emp);
            System.out.println(emp.hashCode());
        }


    }
}
