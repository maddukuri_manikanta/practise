package com.problemsolving.practise.java.comparableandcomparator;

import java.util.Comparator;

class AdressComparator implements Comparator<Address> {

        @Override
        public  int compare(Address a1,Address a2){
            return a1.getId()-a2.getId();
        }
}


class AddressCityComparator implements  Comparator<Address> {
    @Override
    public int compare(Address o1, Address o2) {
        return o1.getCity().compareTo(o2.getCity());
    }
}

class AddressIDCityComparator implements  Comparator<Address>{
    @Override
    public int compare(Address o1, Address o2) {

        int idCompare = o1.getId() - o2.getId();
        int cityCompare = o1.getCity().compareTo(o2.getCity());

        if(idCompare ==0){
            return (cityCompare == 0) ? (idCompare) : (cityCompare);
        }else{
            return idCompare;
        }
    }
}