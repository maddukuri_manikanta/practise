package com.problemsolving.practise.java.comparableandcomparator;


import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Employee implements  Comparable<Employee>{
    private Integer id;
    private String name;

    @Override
    public int compareTo(Employee employee) {

        this.getId().compareTo(employee.getId());
        return  this.getId();
//        return 0;
    }
    private Address address;
}
