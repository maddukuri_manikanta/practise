package com.problemsolving.practise.java.comparableandcomparator;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Main {


    // Driver code
    public static void main(String[] args) {
    	
    	
        Address address = new Address(10,"chennai");
       

        Address address1 = new Address(3,"banglaore");

        List<Address> addresses = new ArrayList<Address>();
        addresses.add(address);
        addresses.add(address1);
        addresses.add(new Address(3,"hyderabad"));

        System.out.println(Arrays.toString(addresses.toArray()));

        Collections.sort(addresses,Collections.reverseOrder());

        System.out.println(Arrays.toString(addresses.toArray()));

        Collections.sort(addresses,new AddressCityComparator());

        System.out.println(Arrays.toString(addresses.toArray()));

        Collections.sort(addresses,new AddressIDCityComparator());

        System.out.println(Arrays.toString(addresses.toArray()));
    }
}