package com.problemsolving.practise.java.comparableandcomparator;


import lombok.*;

import java.util.Objects;


@NoArgsConstructor
@AllArgsConstructor
@Data
//@Setter
//@Getter
public class Address implements Comparable<Address>{
    private int id;
    private String city;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return id == address.id &&
                Objects.equals(city, address.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, city);
    }

    @Override
    public int compareTo(Address address){
        return this.id - address.id;
    }
}