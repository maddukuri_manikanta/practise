package com.problemsolving.practise.java;

public class StringIntern {
    public static void main(String[] args) {
        String s1 = "string1";
        String s2 = "string1";
        String s3 = new String("string1");
        String s4 = s3.intern();

        System.out.println(s1==s2);
        System.out.println(s1==s3);
        System.out.println(s1==s4);
        System.out.println(s3==s4);
        System.out.println(s2==s4);



    }
}
