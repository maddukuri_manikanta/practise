package com.problemsolving.practise;

import java.util.Scanner;

public class Combinations {

    // calculating all combinations and removing combinations which will result adjacent 0's
    public static long cbns(int numberOfDigits){
        long numberOfCombinations = 0;

        // all combinations
        numberOfCombinations = (long) Math.pow(2,numberOfDigits);

        // removing combinations which will result adjacent 0's
        numberOfCombinations = numberOfCombinations - findCBNSWithAdjacentZeros(numberOfDigits);

        return  numberOfCombinations;
    }

    public static long findCBNSWithAdjacentZeros(int n){
        long resultingCombinations = 0;

        // all 0's
        // n-1 digit all 0's
        // ... till 2 digit number

        return resultingCombinations;
    }

    public static void main(String[] args) {
        Scanner in = null;
        try{
            in = new Scanner(System.in);
            int numberOfDigits = in.nextInt();
            System.out.println(cbns(numberOfDigits));
        }catch (Exception e){
            System.err.println(e.getMessage());
        }finally {
            if(in!=null){
                in.close();
            }
        }
    }
}
