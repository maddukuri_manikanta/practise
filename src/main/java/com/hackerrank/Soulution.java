package com.hackerrank;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;



class Result {

    /*
     * Complete the 'starsAndBars' function below.
     *
     * The function is expected to return an INTEGER_ARRAY.
     * The function accepts following parameters:
     *  1. STRING strToEvaluate
     *  2. INTEGER_ARRAY startIndex
     *  3. INTEGER_ARRAY endIndex
     */

    public static int findStarsCount(String str,int start,int end){
        int numberOfStars = 0;
        int temp=0; boolean startCount = false;
        for(int i=start;i<=end;i++){
            if(str.charAt(i)=='|'){
                numberOfStars = numberOfStars + temp;
                temp=0;
                startCount = true;
            }else if(str.charAt(i)=='*' && startCount== true){
                temp++;
            }
        }
        return numberOfStars;
    }

    public static List<Integer> starsAndBars(String strToEvaluate, List<Integer> startIndex, List<Integer> endIndex) {
        // Write your code here
        List<Integer> numberOfStars = new ArrayList<>();

        int start;
        int end;

        for(int i=0;i< startIndex.size();i++){
            start = startIndex.get(i);
            end   = endIndex.get(i);

            // number of stars between start and end
            numberOfStars.add(findStarsCount(strToEvaluate,start,end));
        }
        System.out.println( Arrays.toString(numberOfStars.toArray()));
        return numberOfStars;
    }


    public static void main (String[] args){
        String inputString = "**|**|*|*|*******************|";
        List<Integer> startIndexArray = new ArrayList<>();
        startIndexArray.add(1);
        startIndexArray.add(1);
        List<Integer> endIndexArray = new ArrayList<>();
        endIndexArray.add(5);
        endIndexArray.add(7);

        starsAndBars(inputString,startIndexArray,endIndexArray);

        Collections.unmodifiableList(startIndexArray);
    }

}


