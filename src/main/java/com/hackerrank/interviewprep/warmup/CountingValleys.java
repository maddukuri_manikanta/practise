package com.hackerrank.interviewprep.warmup;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class CountingValleys {


    // Complete the countingValleys function below.
    static int countingValleys(int n, String s) {
        int numberOfValleys = 0;
        int currentPosition = 0;
            for(int i=0;i<n;i++){
                if(s.charAt(i)=='U'){
                    ++currentPosition;
                }else if (s.charAt(i)=='D'){
                    --currentPosition;
                }

                if(currentPosition==0 && s.charAt(i)=='U'){
                    numberOfValleys++;
                }
            }
        return numberOfValleys;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {


        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        String s = scanner.nextLine();

        int result = countingValleys(n, s);

        System.out.println(result);

        scanner.close();
    }
}
