package com.hackerrank.interviewprep.arrays;

import java.util.Arrays;

public class RightRotation {


    public static int[] rightRotation(int[] arr,int numberOfRotataions){

        int length = arr.length;

        if(numberOfRotataions > length) numberOfRotataions = numberOfRotataions % length;

        int temp[] = new int[numberOfRotataions];


        // moving elements from input array to temp array
        for(int i =0;i<numberOfRotataions;i++){
            temp[i]=arr[length-numberOfRotataions+i];
        }

        // moving remaining elements in array
        for (int i=length-numberOfRotataions-1;i>=0;i--){
            arr[i+numberOfRotataions] = arr[i];
        }

        // adding elements from temp array to original array
        for(int i=0;i<temp.length;i++){
            arr[i] = temp[i];
        }

        printArray(arr,"debugging array");
        return arr;
    }

    public static void printArray(int[] arr,String message){
        System.out.println(message+" "+Arrays.toString(arr));
    }

    //driver method
    public static void main(String[] args) {
        int[] arr = new int[]{1,2,3,4,5};
        printArray(arr,"input array");
        printArray(rightRotation(arr,10),"rotated array");
    }
}
