package com.hackerrank.interviewprep.arrays;

public class LeftRotation {

    private static int[] leftRotate(int[] arr,int numberOfRotations){

            if(numberOfRotations > arr.length) numberOfRotations =  numberOfRotations % arr.length;

            int temp[] = new int[numberOfRotations];

            // copying into temporary array
            for(int i=0;i<numberOfRotations;i++){
                temp[i]=arr[i];
            }

            printArray(arr);
            printArray(temp);

            // moving remaining elements to its correct position
            for(int j=0;j<arr.length- numberOfRotations;j++){
                arr[j] = arr[j+numberOfRotations];
            }

            printArray(arr);
            printArray(temp);
            // copying elements from temp array to original array
            for(int i=0;i<temp.length;i++){
                arr[(arr.length)-(numberOfRotations)+i] = temp[i];
            }

            printArray(arr);
            printArray(temp);

            return arr;
    }

    // print array
    private  static void printArray(int[] arr){
        for (int i=0;i<arr.length;i++){
            System.out.print(arr[i]+" ");
        }
        System.out.println();
    }
    // main driver logic
    public static void main(String[] args) {

        int arr[] = new int[]{1,2,3,4,5};
        printArray(arr);
        int rotatedArray[] = leftRotate(arr,6);
        printArray(rotatedArray);
    }
}
