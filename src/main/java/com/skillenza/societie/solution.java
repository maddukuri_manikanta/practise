package com.skillenza.societie;

import java.util.Scanner;

public class solution {

	public static void main(String[] args) {
		try (Scanner in = new Scanner(System.in)) {
			int count = in.nextInt();

			for (int i = 0; i < count; i++) {

				int m = in.nextInt();
				int n = in.nextInt();

				char[][] matrix = new char[m][n];
				for (int j = 0; j < m; j++) {
					String row = in.next();

					matrix[j] = row.toCharArray();
				}
				char[][] parsed = parse(matrix, m, n);
				System.out.println(getCount(parsed, m, n));
			}
		}
	}

	static char[][] parse(char[][] a, int m, int n) {

		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {

				char c = a[i][j];

				if (c != 'r' || c != 'b') {

					// check if horizontal and vertical adjacent is not b
					// left
					if (j - 1 >= 0) {
						if (a[i][j - 1] == 'r' || a[i][j - 1] == 'b') {
							if (a[i][j] != 'b' && a[i][j] != 'r')
								a[i][j] = 'o';
						}
					}

					// right
					if (j + 1 < n) {
						if (a[i][j + 1] == 'r' || a[i][j + 1] == 'b') {
							if (a[i][j] != 'b' && a[i][j] != 'r')
								a[i][j] = 'o';
						}
					}

					// top
					if (i - 1 >= 0) {
						if (a[i - 1][j] == 'r' || a[i - 1][j] == 'b') {
							if (a[i][j] != 'b' && a[i][j] != 'r')
								a[i][j] = 'o';
						}
					}

					// bottom
					if (i + 1 < m) {
						if (a[i + 1][j] == 'r' || a[i + 1][j] == 'b') {
							if (a[i][j] != 'b' && a[i][j] != 'r')
								a[i][j] = 'o';
						}
					}

					// diagonal top left
					if (i - 1 >= 0 && j - 1 >= 0) {
						if (a[i - 1][j - 1] == 'r') {
							if (a[i][j] != 'b' && a[i][j] != 'r')
								a[i][j] = 'o';
						}
					}

					// diagonal top right
					if (i - 1 >= 0 && j + 1 < n) {
						if (a[i - 1][j + 1] == 'r') {
							if (a[i][j] != 'b' && a[i][j] != 'r')
								a[i][j] = 'o';
						}
					}

					// diagonal bottom left
					if (i + 1 < m && j - 1 >= 0) {
						if (a[i + 1][j - 1] == 'r') {
							if (a[i][j] != 'b' && a[i][j] != 'r')
								a[i][j] = 'o';
						}
					}

					// diagonal bottom right
					if (i + 1 < m && j + 1 < n) {
						if (a[i + 1][j + 1] == 'r') {
							if (a[i][j] != 'b' && a[i][j] != 'r')
								a[i][j] = 'o';
						}
					}
				}

			}
		}
		return a;
	}

	static int getCount(char[][] a, int m, int n) {
		int count = 0;

		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				if (a[i][j] != 'o' && a[i][j] != 'r' && a[i][j] != 'b') {
					count++;
				}
			}
		}
		return count;
	}

}