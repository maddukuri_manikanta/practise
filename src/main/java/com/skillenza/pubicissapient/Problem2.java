package com.skillenza.pubicissapient;

import java.util.*;

public class Problem2 {
    public static void main(String args[] ) throws Exception {

        Scanner sc = new Scanner(System.in);
        
        long L = 0, R = 0;
        int STEP_SIZE = 0;
        
        L = sc.nextLong();
        R = sc.nextLong();
        STEP_SIZE = sc.nextInt();
        
        int N = sc.nextInt();
        long arr[] = new long[N];
        for(int i=0; i<N; i++)
            arr[i] = sc.nextLong();
        solve(L, R, STEP_SIZE, arr, N);
    }
    
    public static void solve(long L, long R, int STEP_SIZE, long[] arr, long N){
        Arrays.sort(arr);
        long max =  arr[(int) N-1];
        
        // Left leg faced mines
        System.out.print(legCount(L,max,arr,STEP_SIZE)+" ");
        
        // Right leg faced mines
        System.out.print(legCount(R,max,arr,STEP_SIZE));
    }
    
    public static long legCount(long leg,long max,long[] arr,int STEP_SIZE){
        // left leg faced mines
        long count = 0;
        while(leg<=max){
           long ind = Arrays.binarySearch(arr,leg);
           if(ind>=0){
            count = count + numberOfMinesAtSameIndex(arr,ind,leg);
           }
           leg = leg + (STEP_SIZE);
        }
        return count;
    }
    
    
    public static long numberOfMinesAtSameIndex(long[] a, long index, long key){
        
        // count left side from index
        long count =1;
        long left = index-1;
        while(left>=0 && a[(int)left]==key){
            count++;
            left--;
        }
        
        // count right side from index
        long right = index+1;
        while(right<a.length && a[(int)right]==key){
            count++;
            right++;
        }
        
        return count;
    }
    
    
	/*
	 * sample input
	 * 1 2 3 
	 * 8 
	 * 16 10 13 4 8 7 15 20
	 * 
	 * sample output
	 * 5 2
	 */
}

