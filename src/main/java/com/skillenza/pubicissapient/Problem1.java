package com.skillenza.pubicissapient;

import java.util.*;
public class Problem1 {
    public static void main(String args[] ) throws Exception {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        long arr[] = new long[N];
        for(int i=0; i<N; i++)
            arr[i] = sc.nextLong();
        long out = solve(arr, N);
        System.out.println(out);
    }
    
    public static long solve(long[] arr, long N){
        long totalRoads = 0;
        Arrays.sort(arr);
        
        for(int i=0;i<N;i++){
            
            long square = (arr[i]*arr[i]);
            int ind = Arrays.binarySearch(arr,square);
           
            if(ind >=0 && i!=ind){
               totalRoads = totalRoads + countRoads(arr,ind,square);
            }
            
        }
        return totalRoads;
    }
    
   public static int countRoads(long[] a,int index,long key){
       
       // count left from index
       int count = 1;
       int left = index-1;
       while(left>=0 && a[left]==key){
           count++;
           left--;
       }
       
       // count right from index
       int right = index+1;
       while(right<a.length && a[right]==key){
           count++;
           right++;
       }
       
       return count;
   }
    
    
}



/*
 * 
 * sample input
 * 7 1 2 2 4 4 7 4
 * 
 * 
 * sample output
 * 6
 */