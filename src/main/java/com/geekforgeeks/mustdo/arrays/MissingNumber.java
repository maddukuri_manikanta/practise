package com.geekforgeeks.mustdo.arrays;

/*package whatever //do not write package name here */

import java.util.*;
import java.lang.*;
import java.io.*;

public class MissingNumber {


    private static int findMissingElement(int[] arr,int length){

        int actualSum = 0;
        for(int i=0;i<arr.length;i++){
            actualSum = actualSum + arr[i];
        }

        int expectedSum = (length * (length+1))/2;
        return expectedSum-actualSum;
    }

    public static void main (String[] args) throws IOException {

        try(BufferedReader in = new BufferedReader(new InputStreamReader(System.in))){
            int numberOfCases = Integer.parseInt(in.readLine());

            for(int i=1;i<=numberOfCases;i++){
                int arraySize = Integer.parseInt(in.readLine());
                int[] arr = new int[arraySize-1];


                String[] strings = in.readLine().trim().split("\\s+");

                for(int j=0;j<strings.length;j++){
                    arr[j] = Integer.parseInt(strings[j]);
                }

                System.out.println(findMissingElement(arr,arraySize));
            }
        }


    }

}
