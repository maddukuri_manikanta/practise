package com.geekforgeeks;

public class Kadanes {

    private static int findLargestContiguousSum(int a[]){

        int maxSoFar = a[1];
        int maxEndsHere = 0;
        int start=0,end=0,s=0;

        for(int i=0;i<a.length;i++){
            maxEndsHere = maxEndsHere + a[i];

            if(maxSoFar < maxEndsHere){
                maxSoFar = maxEndsHere;
                start = s;
                end = i;
            }

            if(maxEndsHere<0){
                maxEndsHere =0;
                s = i+1;
            }
        }

        System.out.println("start "+start+" end "+end);
        return maxSoFar;
    }

    public static void main(String[] args) {
        int[] arr= new int[]{-1,2,3,-2,4,-9,-5,5,-10};

        System.out.println(findLargestContiguousSum(arr));
    }
}
